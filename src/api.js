import axios from 'axios';
import config from './config';

export default {
  getContactConfig() {
    return  axios.get(config.rootPath + "/contact-config");
  },
  sendClientIformation(data) {
    return  axios.post(config.rootPath + "/contact-requests", data);
  }

}