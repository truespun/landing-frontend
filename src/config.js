const config = {};

if (typeof process.env.REACT_APP_PORT === 'string') {
  config.port = process.env.REACT_APP_PORT;
}

if (typeof process.env.REACT_APP_PATH === 'string') {
  config.path = process.env.REACT_APP_PATH;
}

config.rootPath = config.path + config.port;

export default config;