import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header/Header";
import TypeService from "./components/TypeService/TypeService";
import Salon from "./components/Salon/Salon";
import Advantages from "./components/Advantages/Advantages";
import StepWorks from './components/StepWorks/StepWorks';
import Calculator from './components/Calculator/Calculator';
import Partner from './components/Partner/Partner';
import Footer from './components/Footer/Footer';
import "./dev/fontawesome/css/fontawesome.css";

import Map from "./components/Map/Map";
import api from './api';

class App extends Component {
  state = {
    contact: []
  };

  componentDidMount() {
    api.getContactConfig()
    .then(res => {
      const contact = res.data;
      this.setState({contact: contact[0]});
    })
  };


    render() {
      const {contact} = this.state;
        return (
            <div className="App">
                <Header data={contact} id='section0'/>

                <TypeService id="section1"/>

                <Salon id="section2" />

                <Advantages id="section3"/>

                <StepWorks id="section4"/>

                <Calculator id='section5'/>

                <Partner/>

                <Map id="section6" contact={contact}/>

                <Footer/>

            </div>
        );
    }
}

export default App;
