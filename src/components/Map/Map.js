import React,{Component} from "react";
import styled from "styled-components";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import api from '../../api';
import './map.scss'



const MainDiv = styled.div`
    height: 72vh;
    @media (min-width: 320px) and (max-width: 790px) {
      height: 100vh;
    }
`;


const Contact = styled.div`
    width: 80%;
    height: auto;
    position: absolute;
   
    margin-top: 66px;
    background-color: transparent;
    z-index: 20;
    display: flex;
    flex-direction: column;
    padding-left: 165px;
    padding-bottom:10px;
    
     @media (min-width: 790px) and (max-width: 1300px) {
         padding-left:20px;
     }

@media (min-width: 320px) and (max-width: 790px) {
    width: auto;
    height: auto;
    position: relative;
    right:0px;
    background-color: white;
    margin-top: 0px;
    display: flex;
    z-index: 0!important;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    background: black;
    box-shadow: none;
     padding: 30px 20px 30px 20px;
    
    }
`;

const Title = styled.p`
  font-weight: normal;
  font-family: 'Akzidez', sans-serif;
font-size: 44px;
color: white;
margin:0;
margin-right:10px;

@media (min-width: 320px) and (max-width: 790px) {
font-size: 28px;
}



`;

const Span = styled.span`
font-weight: bold;
font-size: 12px;
 font-family: 'Open Sans blod',sans-serif;
color: white;
letter-spacing: 0.1em;
text-transform: uppercase;
opacity: 0.5;

`;

const P = styled.a`
font-weight: bold;
font-size: 24px;
 font-family: 'Open Sans blod',sans-serif;
color: white;
padding-top:0px;
text-decoration: none;
margin-bottom: 20px;

:hover{
  opacity: 0.7;
}

@media (min-width: 320px) and (max-width: 790px) {
font-size: 19px;
}
`;

const MyStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
]


export class Mapr extends Component  {
  state = {
    showingInfoWindow: true,
    activeMarker: {},
    selectedPlace: {},
    // contact: []
  };


  // componentDidMount() {
  //  api.getContactConfig()
  //   .then(res => {
  //     const contact = res.data;
  //     this.setState({contact: contact[0]});
  //   })
  // };

  onMarkerClick = (props, marker, e) =>
      this.setState({
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
      });

  onClose = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };


  render() {
    const {id} = this.props
    const lat = 46.447473
    const lng =  30.732436
    return (

        <MainDiv id={id}>
            <Contact>
              <div className='title-wrapper'>
              <Title className='title'>Контакты</Title>
                <div className='contact-line'/>
              </div>
              <Span>Телефон</Span>
               <P href={`tel:${this.props.contact.phone}`}>{this.props.contact.phone || '+380 50 316-34-55'}</P>
              <Span>Email</Span>
              <P href={`mailto:${this.props.contact.email}`}>{this.props.contact.email || 'mail@ontime-auto.com.ua'}</P>
              <Span>Адрес</Span>
              <P href={`http://maps.google.com/maps?daddr=${lat},${lng}&ll=`}>{this.props.contact.address || 'ул. Краснова, 2/1, г. Одесса'}</P>
            </Contact>
            <Map google={this.props.google}
                 initialCenter={{
                   lat: 46.447473,
                   lng: 30.732436
                 }}
                 styles={MyStyle}
                 className={'map'}
                 zoom={16}
                 fullscreenControl={false}
                 mapTypeControl={false}
                 clickableIcons={false}
                 // disableDoubleClickZoom={false}
                 streetViewControl={false}
                 // scrollwheel={false}
                 zoomControl={false}
                 // noClear={false}
                 // draggable={false}
                 keyboardShortcuts={false}>
              <Marker onClick={this.onMarkerClick}  name={'Мы находимся тут'}/>

              <InfoWindow marker={this.state.activeMarker}
                          visible={this.state.showingInfoWindow}
                          onClose={this.onClose}>
                <div>
                  <h4>{this.state.selectedPlace.name}</h4>
                </div>
              </InfoWindow>
            </Map>
        </MainDiv>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: ('AIzaSyCSl773A20fzagSoTSTh0xAW8klmQZ21IE')
})(Mapr)
