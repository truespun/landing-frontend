import React from "react";
import styled from 'styled-components';
import salon from '../../dev/img/salon.jpg';
import auction from '../../dev/img/auction.jpg';
import credit from '../../dev/img/credit.jpg';
import './salon.scss'

const SalonWrapper = styled.div`
    height: 65vh;
    background: grey;
    width: 100%;
    
    @media (min-width: 320px) and (max-width: 790px) {
      height: 100%;
      width:100%;
      
    
    
        }   
    `;

const MainDiv = styled.div`
     height: 100%;
     width: 100%;
     display:flex;
     
     
     
      @media (min-width: 320px) and (max-width: 790px) {
      height: 100%;
      width:100%;
      display:flex;
      flex-direction:column;
      flex-wrap: wrap;
    
    
        }
        
    `;
const SalonBG = styled.div`
    width: 100%;
    height: 100%;
    background-size: cover;
    
    background-image: url(${salon});
    background-repeat: no-repeat;
    background-position: left bottom;
    
    
    display:flex;
    justify-content: start;
   
        
    @media (min-width: 320px) and (max-width: 790px) {
    width: 100%
    height: 42vh;
    background-size: 100%;
    background-position: top;
        }
    `;
const SalonText = styled.div`
    width: 63%;
    height: 40%;
    padding-left:165px
    padding-top: 66px;
    
     @media (min-width: 790px) and (max-width: 1300px) {
         padding-left:20px;
       
     }
    
     @media (min-width: 320px) and (max-width: 790px) {
    width: 80%
    margin-left: 0;
    padding-left:20px;
     padding-top: 30px;
    
        }
   
`;

const TextH1 = styled.h1`
    font-family: Open Sans blod;
    font-style: normal;
    font-weight: bold;
    font-size: 30px;
    margin: 0;
    color: #FFFFFF;
`;

const TextP = styled.p`
    font-family: Open Sans blod;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;

    color: #FFFFFF;
    
     @media (min-width: 320px) and (max-width: 790px) {
    display:none;
        }
`;


const ACwrapper = styled.div`
    width: 50%;
    height: 100%;
   @media (min-width: 320px) and (max-width: 790px) {
    width:100%
    height: 100%;
        }
`;


const Auction = styled.div`
    width:100%;
    height:100%;
    background-size: 100%;
    background-image: url(${auction});
    background-repeat: no-repeat;
    background-position: center;
    display:flex;
    justify-content:center;
    
    @media (min-width: 320px) and (max-width: 790px) {
    width:100%
    height: 38vh;
    background-size: 100%;
    
  
    }

`;
const AuctionText = styled.div`
    width: 52%;
    height: 50%;
    padding-top: 66px;
    margin-right:190px;
     font-family: 'Open Sans blod',sans-serif;
    font-style: normal;
    font-weight: bold;

    
   @media (min-width: 320px) and (max-width: 790px) {
    padding-left:20px;
     width: 80%;
      padding-top: 20px;
        margin-left: 0;
         margin-right:0px;
    }
`;

const Credit = styled.div`
    width:100%;
    height:100%;
    background-image: url(${credit});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    display:flex;
    justify-content:center;
    
   
    
    
    @media (min-width: 320px) and (max-width: 790px) {
    width:100%
    height: 42vh;
    background-size: 100%;
    }
`;


const Salon = ({id}) => {

    return (
        <SalonWrapper id={id}>
            <MainDiv>
                <div className='salon-wrapper'>
                <SalonBG className='credit'>
                    <SalonText>
                        <TextH1>Салон в центре Одессы</TextH1>
                        <TextP>Наш красивый салон удобно расположен в центре Одессы,
                            на улице Краснова (скоростной трассе), недалеко от Среднефонтанской площади,
                            возле перекрестка с Артиллерийским переулком.
                            Напротив салона есть светофор, позволяющий легко подъехать к салона с любого направления.</TextP>
                    </SalonText>
                </SalonBG>
                </div>
                <ACwrapper>
                    <div className='auction-wrapper'>
                    <Auction className='credit'>
                        <div className='auction-mask'>
                        <AuctionText className='text'>
                        <TextH1>Торги онлайн в салоне</TextH1>
                        <TextP>Вы можете принять участие в аукционах прямо в нашем салоне,
                            в удобном офисе и с помощью нашего персонала.</TextP>
                        </AuctionText>
                        </div>
                    </Auction>
                    </div>
                    <div className='credit-wrapper'>
                    <Credit className='credit'>
                        <div className='credit-mask'>
                        <AuctionText>
                            <TextH1>Кредитование</TextH1>
                            <TextP>У нас есть возможность оформить покупку автомобиля в кредит на отличных условиях.</TextP>
                        </AuctionText>
                        </div>
                    </Credit>
                    </div>
                </ACwrapper>


            </MainDiv>


        </SalonWrapper>

    );
};

export default Salon;