import React, { Component } from 'react';

class Menu extends React.Component {
  constructor(props){
    super(props);
    this.state={
      open: this.props.open? this.props.open:false,
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.open !== this.state.open){
      this.setState({open:nextProps.open});
    }
  }

  render(){
    const styles={
      container: {
        position: 'fixed',
        top: 0,
        left: 0,
        height: this.state.open? '100%': 0,
        width: '100vw',
        display: 'flex',
        background: 'rgba(0,0,0,0.8)',
        color: 'white',
        transition: 'height 0.3s ease',
        zIndex: 2,
      },
      menuList: {
        paddingTop: '3rem',
      }
    }
    return(
        <div style={styles.container} className='MobMenu'>
          {
            this.state.open?
                <div style={styles.menuList} className='Menu-wrapper'>
                  {this.props.children}
                </div>:null
          }
        </div>
    )
  }
}

export default Menu