import React, { Component } from 'react';
import {Link} from 'react-scroll';

class MenuItem extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      hover:false,
      // openModal: false
    }

    // this.handleTableViewBtn = this.handleTableViewBtn.bind(this);
  }

  handleHover(){
    this.setState({hover:!this.state.hover});
  }


  //  toggleViewModal (e, classNameToTrigger){
  //   if (e && classNameToTrigger && e.target.className.indexOf(classNameToTrigger) === -1) return;
  //
  //   if (!this.state.openModal) {
  //     this.setState({openModal: true})
  //   } else {
  //     this.setState({openModal: false})
  //   }
  // };
  //
  // renderView (){
  //   return (
  //       <div
  //           className={`modal-view-container ${this.state.openModal ? 'show' : ''}`}
  //           onClick={e => this.toggleViewModal(e, 'modal-view-container')}
  //       >
  //         <div className="modal-view-content">
  //           <Modal/>
  //           <i className="fal fa-times fa-2x" onClick={this.toggleViewModal}> </i>
  //
  //         </div>
  //       </div>
  //   );
  // };

  // handleTableViewBtn (){
  // this.state.openModal = true;
  // };

  render(){
    const styles= {
      container: {
        opacity: 0,
        display: 'flex',
        justifyContent: 'center',
        animation: '1s appear forwards',
        animationDelay: this.props.delay,
      },
      menuItem: {
        fontFamily: `'Open Sans', sans-serif`,
        fontSize: '1.2rem',
        // padding: '1rem 0'
        animationDelay: this.props.delay,
        marginTop: '80px',
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingTop: '20px',
        paddingLeft: '165px',
        paddingRight: '165px',
      },
      line: {
        width: '90%',
        height: '1px',
        background: '#fcaa26',
        animation: '0.5s shrink forwards',
        animationDelay: this.props.delay,
        marginTop: '15px',
        marginBottom: '15px'
      }
    }
    return(
        <div style={styles.container}>
          <div
              style={styles.menuItem}
              onMouseEnter={()=>{this.handleHover();}}
              onMouseLeave={()=>{this.handleHover();}}
          >
            <div className='wrapper-link'>

              <Link className='linkMob'
                activeClass="active"
                to="section1"
                spy={true}
                smooth={true}
                duration={800}
                onClick={this.props.onClick}
            >
              Услуги
            </Link>
              <span>Вы выбираете модель, цвет, комплектацию автомобиля. Если Вы выбрали машину на американском аукционе.</span>
            </div>
            <div className='wrapper-link'>
              <Link className='linkMob'
                activeClass="active"
                to="section3"
                spy={true}
                smooth={true}
                duration={800}
                onClick={this.props.onClick}
            >
              Преимущуства
            </Link>
              <span>Вы выбираете модель, цвет, комплектацию автомобиля. Если Вы выбрали машину на американском аукционе.</span>
            </div>
            <div className='wrapper-link'>
              <Link className='linkMob'
                activeClass="active"
                to="section4"
                spy={true}
                smooth={true}
                duration={800}
                onClick={this.props.onClick}
            >
              Этапы работы
            </Link>
              <span>Вы выбираете модель, цвет, комплектацию автомобиля. Если Вы выбрали машину на американском аукционе.</span>
            </div>
            <div className='wrapper-link'>
              <Link className='linkMob'
                activeClass="active"
                to="section5"
                spy={true}
                smooth={true}
                duration={800}
                onClick={this.props.onClick}
            >
              Калькулятор
            </Link>
              <span>Вы выбираете модель, цвет, комплектацию автомобиля. Если Вы выбрали машину на американском аукционе.</span>
            </div>
            <div className='wrapper-link'>
              <Link className='linkMob'
                    activeClass="active"
                    to="section6"
                    spy={true}
                    smooth={true}
                    duration={800}
                    onClick={this.props.onClick}
              >
                Контакты
              </Link>
              <span>Вы выбираете модель, цвет, комплектацию автомобиля. Если Вы выбрали машину на американском аукционе.</span>
            </div>
            <div className='wrapper-icon'>
              <a href={`http://${this.props.contact.facebook}`}>
                <div className='facebook-icon-mob'/>
            </a>
              <a href={`http://${this.props.contact.instagram}`}>
              <div className='instagram-icon-mob'/>
              </a>
            </div>
          </div>

          {/*{this.renderView()}*/}
        </div>
    )
  }
}

export default MenuItem