import React, { Component } from 'react';
import Burger from '../../dev/icon/burger.svg';
import close from '../../dev/icon/close.svg'
import logo from '../../dev/icon/logo.png';

class MenuButton extends React.Component {
  constructor(props){
    super(props);
    this.state={
      open: this.props.open? this.props.open:false,
      color: this.props.color? this.props.color:'black',
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.open !== this.state.open){
      this.setState({open:nextProps.open});
    }
  }

  handleClick(){
    this.setState({open:!this.state.open});
  }

  render(){
    const styles = {
      container: {
        backgroundImage: this.state.open ?`url(${close})`  :`url(${Burger})`,
        height: '25px',
        width: '37px',
        position: this.state.open ? 'fixed' : 'relative',
        // right: this.state.open ? '30px' : '0px',
        backgroundPosition: 'center',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        cursor: 'pointer',

        // display:'flex',
        // flexDirection: 'column',
        // justifyContent: 'center',
        // alignItems: 'center',
        // cursor: 'pointer',
        // background: this.state.open ? 'transparent' : '#ED1C24',
        // borderRadius: '50%',
        // padding: '4px',
      },
      // line: {
      //   height: this.state.open ? '5px' : '2px',
      //   width: this.state.open ? '32px' : '25.5px',
      //   background: this.state.open ?  '#ED1C24' : 'white',
      //   transition: 'all 0.2s ease',
      // },
      // lineTop: {
      //   transform: this.state.open ? 'rotate(41deg)':'none',
      //   transformOrigin: 'top left',
      //   marginBottom: '5px',
      // },
      // lineMiddle: {
      //   opacity: this.state.open ? 0: 1,
      //   transform: this.state.open ? 'translateX(-16px)':'none',
      // },
      // lineBottom: {
      //   transform: this.state.open ? 'translateX(-3px) translateX(0px) rotate(-43deg)':'none',
      //   transformOrigin: 'top left',
      //   marginTop: '5px',
      // },
    }
    return(
        <div style={styles.container}
             className='burger-mobile'
             onClick={this.props.onClick ? this.props.onClick:
                 ()=> {this.handleClick();}}>
          <div style={{...styles.line,...styles.lineTop}}/>
          <div style={{...styles.line,...styles.lineMiddle}}/>
          <div style={{...styles.line,...styles.lineBottom}}/>
        </div>
    )
  }
}

export default MenuButton