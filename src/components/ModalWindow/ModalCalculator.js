import React,{useState} from 'react';
import Validation, {DisabledOnErrors, ValidationInput} from 'react-livr-validation';
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import api from '../../api';

const schema = {
  name: ['alpha_chars', 'not_empty'],
};



const customRules = {
  alpha_chars: function () {
    return function (value) {
      if (typeof value === 'string') {
        if (!/[a-z,A-Z,а-я,А-Я]+/.test(value)) {
          return 'Введите имя';
        }
      }
    };
  }
};


const ModalCalculator = ({sum, autoValue, year, engine, typeEngine}) => {
  const [customerSignUp, setCustomerSignUp] = useState(
      {
        phone: '',
        name: '',
        autoValue: '',
        year: '',
        engine: '',
        typeEngine: '',
        sum: '',
      }
  );
  const [value, setValue] = useState();
  const [submit, setSubmit] = useState(false);


  const handleChangeTel = (value) =>{
    setValue(value)
    setCustomerSignUp({...customerSignUp, phone: value})
  }

  const handleChange = (event) => {
      setCustomerSignUp({...customerSignUp, [event.target.name]: event.target.value,
        autoValue: autoValue,
        year: year,
        engine: engine,
        typeEngine: typeEngine,
        sum: sum
      })
  }


  const handleSubmit = (e) => {
    e.preventDefault()
    setSubmit(true)
    setTimeout(() => setSubmit(false), 3000);
        api.sendClientIformation(customerSignUp)
    .then(function (res) {
      console.log(res)
    })
    .catch(function (error) {
      console.log(error)
    })
  }


  return (
      <Validation
          data={customerSignUp}
          schema={schema}
          rules={customRules}
      >

        <form className="form_block"  onSubmit={handleSubmit}>
          {
            submit ? <div className='submit-wrapper'>
                  <i className="fas fa-check"> </i>
                  <p className='cheking'>Заявка отправлена</p>
                </div>
                :
                <div className='wrapper-form'>
                <h3 id="calc_modal_h3">Оставить заявку</h3>

                <p> Имя </p>
            <ValidationInput name="name">
            <input type="name" name="name"  onChange={handleChange}/>
            </ValidationInput>
            <p>Номер телефона</p>
            <PhoneInput
            placeholder="Введите номер телефона"
            defaultCountry="UA"
            onChange={(value) => handleChangeTel(value)}/>
            <div className="calc_modal_sum_form">
            <p>Сумма</p>
            <ValidationInput name="text">
            <input className="" type='text' name="text" value={sum} placeholder={sum} id='text'/>
            </ValidationInput>
            </div>
            <button className="calc_modal_submit" type="submit">ОТПРАВИТЬ</button>
                </div>
          }
        </form>
      </Validation>

  );
}

export default ModalCalculator;