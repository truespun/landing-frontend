import React,{useState} from 'react';
import Validation, {DisabledOnErrors, ValidationInput} from 'react-livr-validation';
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import api from '../../api'


const Modal = () => {
    const [customerSignUp, setCustomerSignUp] = useState(
        {phone: '', name: ''}
    );
    const [submit, setSubmit] = useState(false);
    const [value, setValue] = useState();


    const schema = {
        name: ['alpha_chars', 'not_empty'],
    };

    const customRules = {
        alpha_chars: function () {
            return function (value) {
                if (typeof value === 'string') {
                    if (!/[a-z,A-Z,а-я,А-Я]+/.test(value)) {
                        return 'Введите имя';
                    }
                }
            };
        }
    };


    const handleChange = (event) => {
        setCustomerSignUp({...customerSignUp, [event.target.name]: event.target.value})
    }

        const handleChangeTel = (value) => {
            setValue(value)
            setCustomerSignUp({...customerSignUp, phone: value})
        }


        const handleSubmit = (e) => {
            e.preventDefault()
            setSubmit(true)
            setTimeout(() => setSubmit(false), 3000);
            api.sendClientIformation(customerSignUp)
        }


        return (
            <Validation
                data={customerSignUp}
                schema={schema}
                rules={customRules}
            >

                <form className="form_block" onSubmit={handleSubmit}>
                    {
                        submit ? <div className='submit-wrapper'>
                                <i className="fas fa-check"> </i>
                                <p className='cheking'>Заявка отправлена</p>
                            </div>
                            :
                            <div className='wrapper-form'>
                                <h3>Оставить заявку</h3>
                                <p>Имя</p>
                                <ValidationInput name="name">
                                    <input type="name" name="name" onChange={handleChange}/>
                                </ValidationInput>
                                <p>Номер телефона</p>
                                <PhoneInput
                                    placeholder="Введите номер телефона"
                                    defaultCountry="UA"
                                    onChange={(value) => handleChangeTel(value)}/>
                                <DisabledOnErrors>

                                    <button type="submit" id="submit" >ОТПРАВИТЬ</button>

                                </DisabledOnErrors>
                            </div>
                    }

                </form>
            </Validation>
        );
}

export default Modal;