import React from "react";
import styled from "styled-components";
import {Title} from "../Calculator/calculatorStyles";


const MainDiv = styled.div`
    height: auto;
  
    display: flex;
    flex-wrap: wrap;
    background: #FF0000;
    padding-left: 165px;
    padding-right: 165px;
    padding-top: 66px;
    padding-bottom: 66px;
    
     @media (min-width: 790px) and (max-width: 1300px) {
         padding-left:20px;
         padding-right:20px;
     }
    
    @media (min-width: 320px) and (max-width: 790px) {
        padding: 10px 0px;
        height: 100%;
        display:flex;
        flex-wrap: wrap;
        }
  `;


const ContentWrpapper = styled.div`
    width:100%
   
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: center;
    
   
    
     @media (min-width: 320px) and (max-width: 790px) {
        width: 100%
        height: 100%;
        flex-wrap: wrap;
        
        }
     
  
}
    `;


const BlockWrapper = styled.div`
   
       height: auto;
        margin-top: 30px;
         display: flex;
        flex-wrap: wrap;
    
       
        @media (min-width: 320px) and (max-width: 790px) {
        width: 100%
        height: 100%; 
        display: flex;
          margin-top: 0px;
        
        
        }
`;

const ContentBlock = styled.div`
    width: 100%;
    height: auto;
    margin: auto;
    display: flex;
    flex-direction: row;
     justify-content: space-between;
   
    
     @media (min-width: 320px) and (max-width: 790px) {
     flex-wrap: wrap;
     width: 80%;
     margin:0;
     padding-left: 20px;
        }
    `;
const TitleWrap = styled.div`

    display: flex;
    flex-direction:row
    
    
`;

const TitleH1 = styled.h1`
    font-family: 'Akzidez';
    font-style: normal;
    font-weight: bold;
    font-size: 44px;
    padding: 0;
    margin:0
    
    color: #FFFFFF;
    
   @media (min-width: 320px) and (max-width: 790px) {
       margin-top: -2px;
       margin: 4% 0;
     font-size: 28px;
       padding-left: 20px;
       }
    `;
const Line = styled.div`
    width: 100%;
    height: 0px;
    border-bottom: 2px solid #FFFFFF;;
    margin: auto 0;
    margin-left: 10px;
    @media (min-width: 320px) and (max-width: 790px) {
      display:none;
       
       }
`;


const TextP = styled.div`
    color: #FFFFFF;
    width:22%;
    font-family: 'Open Sans blod',sans-serif;
    font-weight: bold;
    font-size: 28px;
    margin-right:10px
    line-height: 120%;
    padding:20px 20px 20px 0px;

    
    @media (min-width: 320px) and (max-width: 790px) {
    width: 100%;
     padding:0px 20px 20px 20px;
       font-size: 24px;
        line-height: 120%;
        }
    `;


const Advantages = ({id}) => {

    return (
        <MainDiv id={id}>
            <ContentWrpapper>
                <TitleWrap>
                    <TitleH1>Преимущества</TitleH1>
                    <Line/>
                </TitleWrap>
                <BlockWrapper>

                          <TextP><span>Без комиссий и переплат</span></TextP>

                          <TextP><span>Работа напрямую с аукционами</span></TextP>

                    <TextP><span>Фотоотчет авто перед отправкой</span></TextP>
                  <TextP><span>Прозрачная отчетность</span></TextP>
                  <TextP><span>Гарантия доставки - 2 месяца</span></TextP>
                  <TextP><span>Трекинг контроля доставки</span></TextP>
                </BlockWrapper>
            </ContentWrpapper>

        </MainDiv>
    )

};
export default Advantages;