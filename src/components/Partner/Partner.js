import React from 'react'
import './partner.scss'


const Partner = () => {
  return(
      <div className='wrapper-partner'>
        <div className='mask'>
          <div className='wrapper-title'>
            <span className='title'>Партнер Auto SL</span>
            <div className='line'/>
          </div>
        <div className='content-wrapper'>
          <span>
            Мы тесно сотрудничаем с одним из самых лучших и больших салонов подержанных люксовых авто в Европе.
          </span>
        </div>
        <a className='link' href='https://autosl.de'>Посетить сайт</a>
        </div>
      </div>
  )

}


export default Partner;