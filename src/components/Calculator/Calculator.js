import React, {useState} from 'react';
import Fade from 'react-reveal/Fade';
import Flip from 'react-reveal/Flip';
import Validation, {DisabledOnErrors, ValidationInput} from 'react-livr-validation';
import ModalCalculator from '../ModalWindow/ModalCalculator';
import InputMask from 'react-input-mask';
import { CalculatorWrapper, CalculatorBlock, OperationWrapper,
  InputsWrapper, InputAutoVal, InputTypeEngine,
  Submit, Title, SumbmiteArae, Icon,
MoneyDefault, Text, Label, Sum, Value, SubmiteVAlue, WrapperInp } from './calculatorStyles'
import './calculator.scss'



const Calculator = ({id}) => {
  const [autoValue, setAutoValue] = useState(0);
  const [engine, setEngine] = useState(0);
  const [year, setYear] = useState(0);
  const [typeEngine, setTypeEngine] = useState('Бензин');
  const [sum, setSum] = useState(0);
  const [ openModal, setOpenModal ] = useState(false);

  const [customsFee, setCustomsFee] = useState(0);
  const [EningeTypePrice] = useState({
    'Дизель': { low: 75, high: 150, limit: 3.5 },
    'Бензин': { low: 50, high: 100, limit: 3 },
    'Гибрид': { low: 50, high: 100, limit: 3 },
  })







  const schema = {
    autoValue: ['alpha_chars', 'not_empty'],
    year: ['alpha_year','not_empty'],
    engine: ['alpha_engine','not_empty']
  };

  const customRules = {
    alpha_chars: function () {
      return function (value) {
        if (typeof value === 'string') {
          if (!/[0-9]+/.test(value)) {
            return 'Введите сумму';
          }
        }
      };
    },
    alpha_year: function () {
      return function (value) {
        if (typeof value === 'string') {
          if (!/[0-9]+/.test(value)) {
            return 'Введите год выпуска авто';
          }
        }
      };
    },
    alpha_engine: function () {
      return function (value) {
        if (typeof value === 'string') {
          if (!/[0-9]+/.test(value)) {
            return 'Введите обьём двигателя';
          }
        }
      };
    }
  };


  const handleAutoValueChange = (event) => {
    const price = Number(event.target.value)

    if (price) {
      setAutoValue(price);
      setCustomsFee(price * 0.1);
    }

  };

  const handleEngineChange = (event) => {
    return setEngine(Number(event.target.value));
  };

  const submit = () => {
    if (EningeTypePrice[typeEngine] === undefined){
     return false
    }else {
      const engVal = EningeTypePrice[typeEngine].limit <= engine ? EningeTypePrice[typeEngine].low : EningeTypePrice[typeEngine].high
      const ageMultiplier = new Date().getFullYear() - year
      const extraFee = typeEngine !== 'Гибрид' ? engVal * engine * ageMultiplier : 100
      const nds = (autoValue + (autoValue * 0.1) + extraFee) * 0.2

      const sum = autoValue + customsFee + extraFee + nds

      return setSum(Math.round(sum));
    }

  };

  const handleEngineTypeChange = (event) =>{
    setTypeEngine(event.target.value)

  };

  const handleYearChange = (event) =>{
    if (event.target.value.length > 3) {
     setYear(Number(event.target.value))
    }
  }

  const toggleViewModal = (e, classNameToTrigger) => {
    document.body.style.overflow = 'auto';
    if (e && classNameToTrigger && e.target.className.indexOf(classNameToTrigger) === -1) return;

    if (!openModal) {
      setOpenModal(true);
    } else {
      setOpenModal(false);
    }
  };

  const renderView = () => {
    return (
        <div
            className={`modal-view-container ${openModal ? 'show' : ''}`}
            onClick={e => toggleViewModal(e, 'modal-view-container')}
        >
          <div className="calc-modal-view-content">
            <ModalCalculator  sum={sum}
                              autoValue={autoValue}
                              year={year}
                              engine={engine}
                              typeEngine={typeEngine}/>
            <i className="fal fa-times fa-2x" id="calc_modal_close_button" onClick={toggleViewModal}> </i>

          </div>
        </div>
    );
  };

  const handleTableViewBtn = () => {
    setOpenModal(true);
    document.body.style.overflow = 'hidden';
  };

  const beforeMaskedValueChange = (newState) => {
    let { value, selection } = newState;
    let cursorPosition = selection ? selection.start : null;

    if (value.endsWith('.')) {

      if (cursorPosition === value.length) {
        cursorPosition--;
        selection = {
          start: cursorPosition,
          end: cursorPosition
        };
      }

      value = value.slice(0, -1);
    }

    return {
      value,
      selection
    };
  }

  return (
      <CalculatorWrapper id={id}>
          <Title>
            <span className='title-span'>Калькулятор</span>
            <div className='calculator-line'/>
          </Title>
        <span className='calculator-span'>Заполните поля для того, чтобы рассчитать примерную стоимость</span>
          <OperationWrapper>
            <Validation
                schema={schema}
                rules={customRules}
            >
            <InputsWrapper>
              <WrapperInp>
                <Label>Цена автомобиля, $</Label>
              <ValidationInput name="autoValue">
              <InputMask className='autoValue' mask={'999999999'} maskChar={null} type="text" name='autoValue'  onInput={handleAutoValueChange}/>
              </ValidationInput>
              </WrapperInp>
              <WrapperInp>
                <Label>Год выпуска</Label>
              <ValidationInput name="year">
              <InputMask className='autoValue' maskChar={null} mask={'9999'} type="text" name='year'  onInput={handleYearChange}/>
              </ValidationInput>
              </WrapperInp>
              <WrapperInp>
                <Label>Объем двигателя, куб. см</Label>
              <ValidationInput name="engine">
              <InputMask className='autoValue' mask={'9.99'} beforeMaskedValueChange={beforeMaskedValueChange} maskChar={null} type="text" name='engine'   onChange={handleEngineChange}/>
              </ValidationInput>
              </WrapperInp>
              <WrapperInp>
                <Label>Tип двигателя</Label>
                <div className='select'>
              <InputTypeEngine  onChange={handleEngineTypeChange}>
                <option>Бензин</option>
                <option>Дизель</option>
                <option>Гибрид</option>
              </InputTypeEngine>
                </div>
              </WrapperInp>
            </InputsWrapper>
            </Validation>
            <SumbmiteArae>
              <Submit onClick={submit}>Рассчитать</Submit>
              {
                sum === 0 ? (

                    null
                    )
                    :
                  <Value>
                    <Flip top>
                      <Text>Примерная стоимость:</Text>
                      <Sum>{isNaN(sum) ? 'Введите коректно данные' : sum} $</Sum>
                    </Flip>
                        {/*<SubmiteVAlue onClick={handleTableViewBtn}>Оставить заявку</SubmiteVAlue>*/}
                  </Value>

              }
            </SumbmiteArae>
          </OperationWrapper>
        {renderView()}
      </CalculatorWrapper>
  );
};

export default Calculator;