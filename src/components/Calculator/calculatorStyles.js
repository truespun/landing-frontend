import styled from 'styled-components';
import money from '../../dev/icon/money.png';

export const CalculatorWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: auto;
    padding-left: 165px;
     padding-right: 165px;
     padding-top: 66px;
     padding-bottom: 66px;
    background-color: #FAFAFA;
    
     @media (min-width: 790px) and (max-width: 1300px) {
         padding-left:20px;
         padding-right:20px;
     }
    
     @media (min-width: 320px) and (max-width: 790px) {
      height: auto;
        width: auto;
        padding: 30px 20px 30px 20px;
     }
`;

export const CalculatorBlock = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 62em;
    padding: 10px 70px;
    background-color: white;
    
     @media (min-width: 320px) and (max-width: 790px) {
        width: auto;
        padding: 10px 23px;
     }
  `;


export const OperationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width:100%;
  
    @media (min-width: 320px) and (max-width: 790px) {
    flex-direction: column;
  }
`;

export const InputsWrapper = styled.form`
   display: flex;
     justify-content: space-between;
     flex-wrap: no-wrap;
     
     @media (min-width: 320px) and (max-width: 790px) {
    flex-direction: column;
  }
     
`;

export const InputAutoVal = styled.input`
   width: 300px;
   height: 32px;
   padding: 10px;
   border: none;
   font-size:18px;
   border-radius: 2px;
   outline:none;
  
`;

export const InputTypeEngine = styled.select`
   width: 255px;
   height: 60px;
   padding: 10px 20px 10px 20px;
   border: none;
   border-radius: 2px;
   font-weight: bold;
   font-size: 24px;
   -webkit-appearance: none;
   -moz-appearance: none;
   appearance: none;
   background: #DFD5D6;
   outline:none;
   cursor:pointer;
   
    @media (min-width: 320px) and (max-width: 790px) {
    width: 100%;
  }
   
`;

export const Submit = styled.button`
    width: 255px;
    height: 60px;
    background-color: transparent;
    color: #FF0000;
    border-radius: 2px;
     font-family: 'Open Sans blod',sans-serif;
    font-weight: bold;
    margin-top:35px;
    font-size: 24px;
    border: 2px solid #FF0000;
    outline:none;
    cursor:pointer;
    
     :hover{
  border: 1px solid transparent;
  color: white;
   background-color: #ED1C24;
    -webkit-transition: background-color 500ms linear;
    -ms-transition: background-color 500ms linear;
    transition: background-color 500ms linear;
  }
  
   @media (min-width: 320px) and (max-width: 790px) {
    width: 100%;
     margin-right:0px
     margin-top: 0px
  }
 `;

export const Title = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  font-size: 44px;
  font-family: 'Akzidez', sans-serif;
  font-weight: bold;
  color: #202021;
  margin-bottom: 10px;
  
    @media (min-width: 320px) and (max-width: 790px) {
        font-size: 28px;
     }
  
`;

export const SumbmiteArae = styled.div`
   display: flex;
   align-items: center;
     justify-content: space-between;

   
    @media (min-width: 320px) and (max-width: 790px) {
        width: 100%;
         flex-direction: column;
     }
`;

export const Icon = styled.div`
width:137px;
height: 137px;
background-image: url(${money});
background-position: center;
background-repeat: no-repeat;
background-size: cover;
`;

export const MoneyDefault = styled.div`
   display: flex;
    flex-direction: column;
    align-items: center;
   justify-content: center;
`;

export const Text = styled.p`
 font-family: 'Open Sans blod',sans-serif;
 font-weight: bold;
font-size: 24px;
padding-left:3px;
 margin:0px;
`;

export const Label = styled.label`
font-weight: bold;
font-size: 12px;
 font-family: 'Open Sans blod',sans-serif;
color:#202021;
margin-bottom: 10px;
letter-spacing: 0.1em;
text-transform: uppercase;
`;

export const Sum = styled.div`
    display: flex;
    flex-direction: column;
     font-family: 'Open Sans blod',sans-serif;
    align-items: center;
   justify-content: center;
   font-weight: bold;
  margin-left:5px;
  font-size: 24px;
  color:#202021;
`;


export const Value =styled.div`
 display: flex;
  align-items: center;
   width: 74%;
   margin-top: 35px;
   // padding:10px;
   @media (min-width: 320px) and (max-width: 790px) {
    flex-direction: column;
    width: 100%;
  }
  
`;

export const SubmiteVAlue = styled.button`
   width: 255px;
    height: 60px;
    background-color: transparent;
    color: #FF0000;
    text-transform: uppercase;
    border-radius: 2px;
    font-weight: bold;
    margin-right:20px
    font-size: 20px;
    margin-top: 10px;
   border: 2px solid #FF0000;
    outline:none;
    cursor:pointer;
    
   :hover{
  border: 1px solid transparent;
  color: white;
   background-color: #ED1C24;
    -webkit-transition: background-color 1000ms linear;
    -ms-transition: background-color 1000ms linear;
    transition: background-color 1000ms linear;
  }
  
   @media (min-width: 320px) and (max-width: 790px) {
    width: 100%;
     margin-right:0px
  }
`

export const WrapperInp = styled.div`
    display: flex;
    flex-direction: column;
    margin-right:20px;
    
      @media (min-width: 320px) and (max-width: 790px) {
     height: 120px;
     margin-right:0px
    }
`;