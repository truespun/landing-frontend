import React from "react";
import styled from "styled-components";
import buy from '../../dev/icon/buy.jpg';
import delivery from '../../dev/icon/delivery.jpg';
import sale from '../../dev/icon/sale.jpg';
import tradein from '../../dev/icon/tradein.jpg';


const MainDiv = styled.div`
    height: auto;
    display: flex;
    flex-wrap: wrap;
    padding-left: 165px;
    padding-right: 165px;
    padding-top: 66px;
    padding-bottom: 66px;
    
     @media (min-width: 790px) and (max-width: 1300px) {
         padding-left:20px;
         padding-right:20px;
     }
    
    @media (min-width: 320px) and (max-width: 790px) {
       
        height: 100%;
        display:flex;
        flex-wrap: wrap;
        justify-content: center;
        padding-left: 20px;
        padding-right: 20px;
        padding-top: 30px;
        padding-bottom: 30px;
        }
  `;


const ContentWrpapper = styled.div`
  
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: space-around;
    
     @media (min-width: 320px) and (max-width: 790px) {
       
        height: 100%;
        flex-wrap: wrap;
        }
     
  
}
    `;


const BlockWrapper = styled.div`
       width:100%
       height:auto;
       display: flex;
       flex-direction: row;
       flex-wrap: wrap;
       justify-content: space-between;
       
        @media (min-width: 320px) and (max-width: 790px) {
        height: 100%; 
        display: flex;
         flex-direction: column;
        flex-wrap: wrap;
        
        }
`;

const ContentBlock = styled.div`
    width: 20%;
    height: 100%;
    background: #FFFFFF;
    display: flex;
    flex-direction: column;
    align-items:flex-start;
     padding:0 20px 0 0;
    
     @media (min-width: 320px) and (max-width: 790px) {
     width:90%;
     flex-wrap: wrap;
        }
    `;
const TitleWrap = styled.div`
    display: flex;
    flex-direction:row
    margin-bottom: 30px;
    
     @media (min-width: 320px) and (max-width: 790px) {
        margin-bottom: 10px;
       
       }
`;

const TitleH = styled.h1`
    font-family: 'Akzidez';
   font-style: normal;
    font-weight: bold;
    font-size: 44px;
    line-height: 57px;
    margin:0;
  
   
    
    
    
    color: black;
   @media (min-width: 320px) and (max-width: 790px) {
       margin-top: -2px;
        font-size: 28px;
       
       }
    `;
const Line = styled.div`
    width: 100%;
    height: 0px;
    border-bottom: 2px solid #DFD5D6;
    margin: auto 10px;
    
    @media (min-width: 320px) and (max-width: 790px) {
    margin-top: 7%;
       
       
       }
`;


const TextP = styled.p`
    color: #202021;
    opacity: 0.6;
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 22px;
   
    
    @media (min-width: 320px) and (max-width: 790px) {
    margin:0px;
    margin-top: 15px;
    margin-bottom:15px;
        
        }
    `;


const TextH = styled.h2`
     font-family: 'Open Sans blod',sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 29px;
    margin: 0px
    color: #202021;
    `;




const BuyIcon = styled.div`
    background-size: contain;
    width: 226px;
    height: 39px;
    background-image: url(${buy});
    background-repeat: no-repeat;
     margin-bottom:20px;
    
    @media (min-width: 320px) and (max-width: 790px) {
    display:flex;
   justify-content: flex-start;
    width:100%;
   height: 46px;
      margin-bottom:5px;
      margin-top: 15px;
    }
     `;

const DeliveryIcon = styled.div`
    background-size: contain;
    width: 226px;
    height:46px;
    background-image: url(${delivery});
    background-repeat: no-repeat;
    margin-bottom:13px;
    
    @media (min-width: 320px) and (max-width: 790px) {
     width:100%;
  height: 46px;
     margin-bottom:15px;
     margin-top: 15px;
     }
`;
const SaleIcon = styled.div`
     background-size: contain;
    width: 227px;
    height: 39px;
    background-image: url(${sale});
    background-repeat: no-repeat;
    margin-bottom:20px;
 
    
    @media (min-width: 320px) and (max-width: 790px) {
  height: 46px;
    width:100%;
       margin-bottom:15px;
       margin-top: 15px;
        }
`;
const TradeinIcon = styled.div`
    background-size: contain;
    
    width: 245px;
    height: 39px;
    background-image: url(${tradein});
    background-repeat: no-repeat;
     margin-bottom:20px;
     
    @media (min-width: 320px) and (max-width: 790px) {
    height: 46px;
        width:100%;
     margin-bottom:15px;
     margin-top: 15px;
        }
`;


const TypeService = ({id}) => {

    return (
        <MainDiv id={id}>

            <ContentWrpapper>
                <TitleWrap>
                <TitleH>Услуги</TitleH>
                <Line/>
                </TitleWrap>
                <BlockWrapper>

                    <ContentBlock>
                        <BuyIcon/>

                        <TextH>
                            Покупка
                        </TextH>
                        <TextP>Подбираем авто под ваш бюджет делаем детальный расчет,
                            участвуем в аукционе на онлайн-площадках I.A.A.I,
                            Copart или Manheim.
                            Есть отличные условия для кредитования покупки.</TextP>
                    </ContentBlock>

                    <ContentBlock>
                        <DeliveryIcon/>
                        <TextH>
                            Доставка
                        </TextH>
                        <TextP>Мы пользуемся услугами профессиональных логистических компаний.
                            Их сотрудники на каждом этапе транспортировки ими осуществляется контроль качества выполняемых работ.
                            Предоставляем трекинг для контроля доставки по морю, гарантию доставки в течении 8-9 недель.</TextP>
                    </ContentBlock>

                    <ContentBlock>
                        <SaleIcon/>
                        <TextH>
                            Продажа
                        </TextH>
                        <TextP>Сделаем профессиональные фото и видео, чтобы показать ваше авто с лучшей стороны.
                            Бесплатно разместим объявления на топовых интернет-площадках и нашем сайте.</TextP>
                    </ContentBlock>

                    {/*<ContentBlock>*/}
                        {/*<TradeinIcon/>*/}
                        {/*<TextH>*/}
                            {/*Трейд-Ин*/}
                        {/*</TextH>*/}
                        {/*<TextP>  Обменяем ваш подержанный авто на другой с нашей или вашей доплатой.</TextP>*/}
                    {/*</ContentBlock>*/}

                </BlockWrapper>
            </ContentWrpapper>

        </MainDiv>
    )

};

export default TypeService;

