import React from 'react';
import styled from 'styled-components';
import './stepWorks.scss'


const StepWorks = ({id}) => {
  const StepWorksWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-top:66px;
    padding-bottom:66px;
    height: auto;
    background-color: #DFD5D6;
    
     @media (min-width: 320px) and (max-width: 790px) {
        height: auto;
        justify-content: start;
        padding-right:20px
         padding-top:30px;
        padding-bottom:10px;
     }
    
    
  `;

  const H1wrappe = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 30px;
  `;

  const Title = styled.div`
    display: flex;
    width:100%;
    align-items: center;
  font-size: 44px;
  font-family: 'Akzidez', sans-serif;
  font-weight: bold;
  color: #202021;
  margin-bottom: 15px;
  padding-left:165px;
  padding-right:165px;
  
   @media (min-width: 790px) and (max-width: 1300px) {
         padding-left:20px;
         padding-right:20px;
     }
  
  
   @media (min-width: 320px) and (max-width: 790px) {
        font-size: 28px;
           align-items: baseline;
          padding-left:0;
          padding-right:0;
     }
  
  `;

  const ContentWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    width:100%;
     @media (min-width: 320px) and (max-width: 790px) {
          flex-direction: column;
     }
  `;

  const ContentBlocks = styled.div`
    display: flex;
    width:100%;
    // margin-bottom: 20px;
    
     @media (min-width: 320px) and (max-width: 790px) {
      width: 100%;
      margin-bottom: 15px;
     }
  `;

  const RedBlock = styled.div`
    display: flex;
    background-color: white;
    align-items: center;
    justify-content: center;
    width: 70px;
    height: 70px;
    font-weight: bold;
    font-size: 38px;
    color: black;
    border-radius: 50%
    
     @media (min-width: 320px) and (max-width: 790px) {
    
     }
    
  `;

  const TextBlock = styled.div`
    display: flex;
    width:22%;
    align-items: center;
     font-family: 'Open Sans blod',sans-serif;
    height: 70px;
   font-weight: bold;
    font-size: 24px;
    border-top-right-radius:3px;
    border-bottom-right-radius:3px;
    // padding-top: 30px;
    padding-left: 20px;
    padding-right: 20px;
    color: #202021;
    
     @media (min-width: 320px) and (max-width: 790px) {
     font-weight: bold;
      font-size: 18px;
      width:70%;
     }
  `;

  return (
      <StepWorksWrapper id={id}>
        <H1wrappe>
          <Title>
            <h1 className='title-stepWorks'>Этапы работы</h1>
            <div className='line'/>
          </Title>
        </H1wrappe>

        <ContentWrapper>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='small-line'/>
              <RedBlock>1</RedBlock>
              <TextBlock>Формирование заказа авто</TextBlock>
            </div>
          </ContentBlocks>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='big-line'/>
            <RedBlock>2</RedBlock>
              <TextBlock>Подбор авто на аукционах</TextBlock>
            </div>
          </ContentBlocks>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='small-line'/>
              <RedBlock>3</RedBlock>
              <TextBlock>Проверка авто в системах Carfax и Autocheck</TextBlock>
            </div>
          </ContentBlocks>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='big-line'/>
              <RedBlock>4</RedBlock>
              <TextBlock>Покупка авто на онлайн-торгах</TextBlock>
            </div>
          </ContentBlocks>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='small-line'/>
              <RedBlock>5</RedBlock>
              <TextBlock>Доставка авто</TextBlock>
            </div>
          </ContentBlocks>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='big-line'/>
              <RedBlock>6</RedBlock>
              <TextBlock>Выгрузка и экспедирование авто</TextBlock>
            </div>
          </ContentBlocks>
          <ContentBlocks>
            <div className='wrapper-line'>
              <div className='small-line'/>
              <RedBlock>7</RedBlock>
              <TextBlock>Растаможивание авто</TextBlock>
            </div>
          </ContentBlocks> <ContentBlocks>
          <div className='wrapper-line'>
            <div className='big-line'/>
            <RedBlock>8</RedBlock>
            <TextBlock>Передача авто клиенту с полным пакетом документов</TextBlock>
          </div>
        </ContentBlocks>
        </ContentWrapper>


      </StepWorksWrapper>

  );

};

export default StepWorks;