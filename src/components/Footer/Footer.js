import React from 'react';
import styled from 'styled-components';
import './footer.scss';


const FooterComp = styled.div`
  width: 100%;
  height: 200px;
  background: white;
  display:flex;
   justify-content: center;
    align-items: center;
    
    @media (min-width: 320px) and (max-width: 790px) {
      margin-top: 153px;
  }
}
`;



const Footer = () =>{
  return(
<FooterComp>
  <div className='company-inf'>
    <div className='wrapper-title'>
      <span className='title'>© 2020 Ontime Auto</span>
      <span>Сделано в студии</span>
    </div>
    <div className='inf'>
      <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
        totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
        sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
        sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</span>
    </div>
  </div>
</FooterComp>

  )

}


export default Footer;