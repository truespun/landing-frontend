import React,{useState,useRef,useEffect} from 'react';
import {Link} from 'react-scroll';
import styled from 'styled-components';
import logo from '../../dev/icon/logo.png';
import background from '../../dev/img/bg1.jpeg';
import Modal from '../ModalWindow/Modal';
import Menu from '../MobMenu/Menu';
import MenuItem from '../MobMenu/MenuItem';
import MenuButton from '../MobMenu/MenuButton';
import AOS from 'aos';
import 'aos/dist/aos.css';
import './header.scss';

AOS.init();

const Nav = styled.nav`
    width: 100%;
    height: 80px;
`;


const NavContent = styled.div`
     padding-left:165px;
     padding-right:165px;
    margin: 0 auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 80px;
    
     @media (min-width: 790px) and (max-width: 1300px) {
        display: flex;
        justify-content: space-between;
         padding-left:20px;
         padding-right:20px;
     }
    
     @media (min-width: 320px) and (max-width: 790px) {
       height: 50px;
        display: flex;
        justify-content: space-between;
        padding: 10px 20px;
     }
`;


const Logo = styled.div` 
    background-image: url(${logo});
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;
    width: 319px;
    height: 45px;
    cursor:pointer;
       @media (min-width: 320px) and (max-width: 790px) {
           width: 243px;
          height: 33px;
       }
`;

const Ul = styled.ul`
  display: flex;
  align-items: center;
  
  `;

const SectionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  background-image: url(${background});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  align-items: flex-start;
  
    @media (min-width: 320px) and (max-width: 790px) {
        height:100vh;
       }
`;

const Li = styled.li`
  display: inline;
  color: white;
  font-weight: bold;
  font-size: 16px;
  padding: 10px;
  border-radius: 2px;
  
  // :hover{
  //  background-color: #ED1C24;
  //   -webkit-transition: background-color 1000ms linear;
  //   -ms-transition: background-color 1000ms linear;
  //   transition: background-color 1000ms linear;
  // }
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  color: white;
  padding-left: 165px;
  padding-right: 165px;
  margin-top: 100px;
  
   @media (min-width: 320px) and (max-width: 790px) {
      padding-left: 20px;
      padding-right: 20px;
      justify-content: space-around;
      width: auto;
      margin-top: 0px;
     }
     
      @media (min-width: 790px) and (max-width: 1300px) {
      padding-left: 20px;
      padding-right: 20px;
      // width: auto;
     }
     
@media (min-width: 320px) and (max-width: 568px) {
      padding-left: 20px;
      padding-right: 20px;
      width: 88%;
      align-items: center;
     }
`;

const H1 = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: nowrap;
    font-family: 'Akzidez', sans-serif;
    margin-top: 100px;
    
    @media (min-width: 320px) and (max-width: 568px) {
     margin-top: 0px;
     }
   
`;



const HCon = styled.p`
  font-weight: bold;
    font-size: 50px;
    flex-wrap: nowrap;
     white-space: pre-wrap;
     margin: 0px;
         margin-bottom: 20px;
       @media (min-width: 320px) and (max-width: 790px) {
      font-size: 28px;
         margin-bottom: 0px;
       }
     `;



const MobLink = styled.div`
width: 337px;
height: 60px;
background: transparent;
border-radius: 2px;
font-weight: bold;
font-size: 24px;
padding-left:10px;
padding-right:10px;
color: white;
display: flex;
justify-content: center;
 align-items: center;
 border: 1px solid white
 cursor: pointer;
  font-family: 'Open Sans blod',sans-serif;
 
  :hover{
  border: 1px solid transparent;
   background-color: white;
   color: black
    -webkit-transition: background-color 500ms linear;
    -ms-transition: background-color 500ms linear;
    transition: background-color 500ms linear;
  }
  
  @media (min-width: 320px) and (max-width: 790px) {
  width: 92%;
height: 48px;
font-weight: bold;
font-size: 20px;
margin-top:0px;
      
  }

`;


const Header = ({data, id}) => {
  const [ openModal, setOpenModal ] = useState(false);
  const [menuOpen, setMenuOpen] = useState(false);
  const [isFixed,setFixed] = useState(false)
  const lat = 46.447473
  const lng =  30.732436
  const ref = useRef(null);

  const handleScroll = () => {
    setFixed(ref.current.getBoundingClientRect().top < 0);
  };



 const handleMenuClick = () => {
    setMenuOpen(!menuOpen);
  }

 const handleLinkClick = () =>  {
   setMenuOpen(false)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);


    return () => {
      window.removeEventListener('scroll', () => handleScroll);
    };
  }, []);


  const styles=
      {
        container:{
          top: 33,
          zIndex: '99',
          display:'flex',
          alignItems:'center',
          color: 'white',
          marginRight: '10px',

        },

      };

  const toggleViewModal = (e, classNameToTrigger) => {
    if (e && classNameToTrigger && e.target.className.indexOf(classNameToTrigger) === -1) return;

    if (!openModal) {
      setOpenModal(true);
    } else {
      setOpenModal(false);
      document.body.style.overflow = 'auto';
    }
  };

  const renderView = () => {
    return (
        <div
            className={`modal-view-container ${openModal ? 'show' : ''}`}
            onClick={e => toggleViewModal(e, 'modal-view-container')}
        >
          <div className="modal-view-content">
              <Modal/>
                <i className="fal fa-times fa-2x" onClick={toggleViewModal}> </i>

          </div>
        </div>
    );
  };


  const handleTableViewBtn = () => {
    setOpenModal(true);
    document.body.style.overflow = 'hidden';
  };

    return (
       <SectionWrapper id={id}>
         <div className='stick'>
          <Nav className={`initial ${isFixed ? ' fixed' : ''}`} ref={ref}>
            <NavContent className={`${isFixed ? 'fixed-inner' : ''}`}>
              <Link  activeClass="active"
                     to="section0"
                     spy={true}
                     smooth={true}
                     duration={500}>
                <Logo/>
              </Link>
              <Ul>
                <Li className='display-none'>
                  <a href={`tel:${data.phone}`} className='contact-link'>{data.phone}</a>
                </Li>
                <Li className='display-none'>
                <a href={`http://maps.google.com/maps?daddr=${lat},${lng}&ll=`}  className='contact-link'>{data.address}</a>
                </Li>
                <Li className='display-none'>
                <a href={`http://${data.facebook}`}>
                  <div className='facebook-icon'/>
                </a>
                </Li>
                <Li className='display-none'>
                  <a href={`http://${data.instagram}`}>
                    <div className='instagram-icon'/>
                  </a>
                </Li>
                <Li>
                  <div style={styles.container} className='burger'>
                    <MenuButton open={menuOpen} onClick={()=>handleMenuClick()} color='white'/>
                  </div>
                </Li>
              </Ul>
              <Menu open={menuOpen} className='MobMenu'>
                <MenuItem
                    delay={`${1 * 0.1}s`}
                    onClick={()=>{handleLinkClick();}}
                contact={data}>
                </MenuItem>
              </Menu>
            </NavContent>
          </Nav>
         </div>
         <ContentWrapper>
           <H1><HCon>Автомобили из США и Европы.
             <br/>
             Надежно и вовремя.</HCon>
           </H1>
           <Link
               activeClass="active"
               to="section5"
               spy={true}
               smooth={true}
               duration={500}
               className='link-for-button'
           >
           <MobLink>
            Рассчитать стоимость
           </MobLink>
           </Link>
         </ContentWrapper>
         <div className='sponsor-wrapper'>
           <div className='title'>
                <span>АККРЕДИТАЦИЯ НА ЛУЧШИХ АУКЦИОНАХ</span>
           </div>
           <div className='auction-wrapper'>
             <div className='iaai'/>
             <div className='copart'/>
             <div className='manheim'/>
           </div>
         </div>
         {renderView()}
         <div className='red-line'>

         </div>
       </SectionWrapper>

    );
}

export default Header ;
